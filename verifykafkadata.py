from confluent_kafka import Consumer, KafkaException, KafkaError

# Kafka broker configuration
bootstrap_servers = 'localhost:9092'
topic = 'csv-topic-1'  # Replace with your Kafka topic name

# Kafka consumer configuration
consumer_conf = {
    'bootstrap.servers': bootstrap_servers,
    'group.id': 'kafka-consumer-group',  # Consumer group ID
    'auto.offset.reset': 'earliest'  # Start consuming from the beginning of the topic
}

# Create Kafka consumer instance
consumer = Consumer(consumer_conf)
consumer.subscribe([topic])

try:
    while True:
        msg = consumer.poll(1.0)

        if msg is None:
            continue
        if msg.error():
            if msg.error().code() == KafkaError._PARTITION_EOF:
                continue
            else:
                raise KafkaException(msg.error())

        # Print the received message value
        print(f"Received message: {msg.value().decode('utf-8')}")

except KeyboardInterrupt:
    pass
finally:
    # Close the Kafka consumer to free up resources
    consumer.close()

