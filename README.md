# data-engineering-project



## Getting started

To make it easy for installing and making sure my project for the data engineering project is working as expected its neccesary to follow those steps.

I used my Ubuntu:22 LTD labtop for this project, so you need to check your OS requirement also.

## The installation steps:

- 1 Clone my repo: git clone https://gitlab.com/eman.saeed/data-engineering-project.git

- 2 Go inside the folder cloned and run this command in terminal: sudo docker-compose up ( This will start pulling all the required services and versions from docker to your cluster)

![Containers](images/containers.png)

- 3 After all services of docker installed and the docker service running (which you can check using: sudo docker ps)  then you'll be able to check them in your localhost

- 4 Go to localhost:50070 to check the Hadoop data node and its HDFS

- 5 Go to localhost:5601 to check Kibana interface of elasticsearch.

- 6 if hadoop and elasticsearch cluster running and accessable, then fo a head to upload data.

- 7 We hava a bankdata.csv file downloaded from https://www.kaggle.com/datasets/ksabishek/massive-bank-dataset-1-million-rows?resource=download which attached in this repo also.

- 8 Run the automated pipeline which will upload this csv to Kafka as a topic then consume this data topic and upload it to data directory in HDFS

- 8.1 you only you need to run the main python, some libraries required to run this, which they are:

- 8.2 Python3, numpy, confluent_kafka make sure to install them using pip3.

- 8.3 In the same repo directory run this command :  python3 mainKafkaHDFS.py

- 8.4 After script ran successfully, You can use my script in other python file to verify that data exists in the topic named in the file which in this case(csv-topic-1) by running :
	 python3 verifykafkadata.py , this will start logging the data of the topic if uploaded.
	 
	![Kafka Data](images/kafkadata.png)

- 8.5 Verify the HDFS data on the hadoop node

	![Hadoop Node](images/data.png)

	![Data Node](images/data_hdfs.png)

- 9 Then we need to upload data from our HDFS to elasticsearch cluser, here i user a jar library to work as a connector :https://artifacts.elastic.co/downloads/elasticsearch-hadoop/elasticsearch-

- 9.1 This jar file alreadt attached in this repo, so go to next step:

- 9.2 Copy the jar connector to Hadoop : docker cp /home/eman/Documents/data-engineering-project/elasticsearch-hadoop-7.10.1.jar new-hadoop-namenode-1:/lib

	Don't forget to update the path to your filesystem path.

- 9.3 To Read from HDFS and Write to Elasticsearch, i needed to use a MapReduce job and execute it inside the Hadoop cluster.

- 9.4 The MapReduce Job file is written in Java and attached (Thanks to online communities and stackoverflow to help on completing this Map reduce job and make it works) in this file on this repo HDFSToElasticsearch.java.

- 9.5 To compile and run the job follow those steps:

- 9.5.1 On new terminal run this: docker cp HDFSToElasticsearch.java new-hadoop-namenode-1:/tmp/HDFSToElasticsearch.java

- 9.5.2 docker exec -it new-hadoop-namenode-1 /bin/bash

- 9.5.3 cd /tmp

- 9.5.4 mkdir -p classes

- 9.5.5 javac -classpath $(hadoop classpath):/lib/elasticsearch-hadoop-7.10.1.jar -d classes HDFSToElasticsearch.java jar -cvf HDFSToElasticsearch.jar -C classes .

- 9.5.6 Run the job: hadoop jar /tmp/HDFSToElasticsearch.jar HDFSToElasticsearch /user/root/csv_data/data.csv

- 10 Once job executed, you can access the data on elasticsearch Kabana by creating index pattern and access it to visualize the data.

![Map reduce job](images/job.png)

- 10.1 And also you can create index for the HDFS data using : curl -X PUT "localhost:9200/hdfs_data" -H 'Content-Type: application/json' -d '{
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 1
    },
    "mappings": {
        "properties": {
            "field1": { "type": "text" },
            "field2": { "type": "keyword" },
            "timestamp": { "type": "date" }
            // Add more fields as per your data structure
        }
    }
}'

![Acknowledge](images/ack.png)

- 10.2  You can check the indexes exists using: curl -X GET "localhost:9200/_cat/indices?v"

- 10.3 Then go to kabana localhost and go to option: Index Patterns, create an index which you'll be able to list and see you're index named: hdfs_data_index

- 10.4 If you faced error for time-out, check the elasticsearch cluster: i found this error ""stacktrace": ["org.elasticsearch.cluster.block.ClusterBlockException: index [.kibana-event-log-7.10.1-000001] blocked by: [TOO_MANY_REQUESTS/12/disk usage exceeded flood-stage watermark, index has read-only-allow-delete block];"," 

    then i needed to scale up my configuration for elastic cluster using this:
    
	curl -X PUT "http://localhost:9200/_cluster/settings" -H 'Content-Type: application/json' -d '{
	  "transient": {
	    "cluster.routing.allocation.disk.watermark.low": "85%",
	    "cluster.routing.allocation.disk.watermark.high": "90%",
	    "cluster.routing.allocation.disk.watermark.flood_stage": "95%"
	  }
	}'

 And i see after restarted my cluster that the status of elastic changed from red to yellow:
 
 ![Elastic Timeout fix](images/yel.png)
 
- 10.4 Once index created the data can be accessed and visualized.

![Kabana Local](images/kib.png)
and visulize it by selecting columns from the available columns of the CSV:
![Kabana Visualize my data](images/vis.png)


