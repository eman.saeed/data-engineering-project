import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.elasticsearch.hadoop.mr.EsOutputFormat;
import org.elasticsearch.hadoop.mr.LinkedMapWritable;
import org.elasticsearch.hadoop.mr.WritableArrayWritable;

public class HDFSToElasticsearch {
    
    public static class HDFSToESMapper extends Mapper<LongWritable, Text, WritableArrayWritable, LinkedMapWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] fields = value.toString().split(",");
            LinkedMapWritable doc = new LinkedMapWritable();
            doc.put(new Text("field1"), new Text(fields[0]));
            doc.put(new Text("field2"), new Text(fields[1]));
            context.write(null, doc);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("es.nodes", "elasticsearch:9200");
        conf.set("es.resource", "index/type");
        conf.set("es.input.json", "true");

        Job job = Job.getInstance(conf, "HDFS to Elasticsearch");
        job.setJarByClass(HDFSToElasticsearch.class);
        job.setMapperClass(HDFSToESMapper.class);
        job.setOutputFormatClass(EsOutputFormat.class);
        job.setOutputKeyClass(WritableArrayWritable.class);
        job.setOutputValueClass(LinkedMapWritable.class);
        job.setNumReduceTasks(0);
        job.setOutputFormatClass(NullOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

