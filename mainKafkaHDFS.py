from confluent_kafka import Consumer, Producer, KafkaException, KafkaError
import csv
from hdfs import InsecureClient
import pyarrow as pa
import pyarrow.csv as csv_writer

bootstrap_servers = 'localhost:9092'
topic = 'csv-topic-1'


hdfs_host = 'localhost'
hdfs_port = 50070
hdfs_user = 'root' 
hdfs_path = '/user/root/csv_data_new'  

producer = Producer({'bootstrap.servers': bootstrap_servers})


def produce_from_csv(csv_file):
    with open(csv_file, newline='') as f:
        reader = csv.reader(f)
        for row in reader:

            message = ','.join(row)
            producer.produce(topic, message.encode('utf-8'))
            producer.flush()  

    print("Write the banck CSV data to Kafka topic:", topic)


def consume_and_upload():
    consumer_conf = {
        'bootstrap.servers': bootstrap_servers,
        'group.id': 'kafka-consumer-group',
        'auto.offset.reset': 'earliest' 
    }

    consumer = Consumer(consumer_conf)
    consumer.subscribe([topic])

    try:
        while True:
            msg = consumer.poll(1.0)

            if msg is None:
                continue
            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    continue
                else:
                    raise KafkaException(msg.error())

            #  the CSV 
            csv_data = msg.value().decode('utf-8')
            rows = [row.split(',') for row in csv_data.split('\n')]

            # Data to HDFS
            file_name = f"data.csv" 
            upload_to_hdfs(file_name, rows)

    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()

def upload_to_hdfs(file_name, data):
    hdfs_client = InsecureClient(f'http://{hdfs_host}:{hdfs_port}', user=hdfs_user)
    print(":::::::::::::::::::::print is indside the hooost",hdfs_client)
    hdfs_file = hdfs_path + file_name

    # Convert data to a Pandas DataFrame (for demonstration)
    df = pa.Table.from_pydict({f"column_{i}": col for i, col in enumerate(zip(*data))})

    # Write DataFrame to HDFS
    with hdfs_client.write(hdfs_file, overwrite=True) as writer:
        csv_writer.write_csv(df, writer)

    print(f"CSV data uploaded to HDFS: {hdfs_file}")


if __name__ == '__main__':
    csv_file = '/home/eman/Documents/data-engineering-project/bankdataset.csv'
    produce_from_csv(csv_file)
    consume_and_upload()

